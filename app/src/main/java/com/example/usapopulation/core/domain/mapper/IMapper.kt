package com.example.usapopulation.core.domain.mapper

interface IMapper<From, To> {

    fun map(from: From): To

    fun map(from: List<From>): List<To> = from.map { map(it) }
}
