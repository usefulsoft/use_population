package com.example.usapopulation.core.presentation

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

open class BaseViewModel : ViewModel() {

    val msProgress = mutableStateOf(false)
    val msError = mutableStateOf<Int?>(null)

    protected fun jobIO(
        action: suspend () -> Unit
    ) {

        viewModelScope.launch {
            msProgress.value = true

            withContext(Dispatchers.IO) {
                action.invoke()
            }

            msProgress.value = false
        }
    }

    protected fun jobMain(
        operation: suspend () -> Unit
    ) {
        viewModelScope.launch {
            msProgress.value = true
            operation.invoke()
            msProgress.value = false
        }
    }
}
