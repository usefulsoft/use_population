package com.example.usapopulation.core.data.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import dagger.hilt.android.qualifiers.ActivityContext

class NetworkState(
    @ActivityContext private val activity: Context
) {

    var isNetworkAvailable = false

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {

        override fun onAvailable(network: Network) {
            isNetworkAvailable = true
        }

        override fun onLost(network: Network) {
            isNetworkAvailable = false
        }
    }

    fun registerNetworkStateObserver() {
        (activity.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager)?.registerDefaultNetworkCallback(
            networkCallback
        )
    }

    fun unregisterNetworkStateObserver() {
        (activity.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager)?.registerDefaultNetworkCallback(
            networkCallback
        )
    }
}
