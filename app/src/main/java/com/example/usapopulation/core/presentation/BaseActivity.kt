package com.example.usapopulation.core.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.usapopulation.core.data.network.NetworkState
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

open class BaseActivity : AppCompatActivity() {

    @ActivityScoped
    @Inject
    lateinit var networkState: NetworkState

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        networkState.registerNetworkStateObserver()
    }

    override fun onDestroy() {
        networkState.unregisterNetworkStateObserver()
        super.onDestroy()
    }
}
