package com.example.usapopulation.core.data.network

import com.example.usapopulation.BuildConfig
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient(
    private val gson: Gson
) {

    private var sessionState: SessionState = SessionState(
        baseUrl = BuildConfig.BASE_URL,
        authorized = false
    )

    private val okHttpClient: OkHttpClient
        get() = OkHttpClient()
            .newBuilder()
            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            .build()

    private var retrofitCached: Retrofit = updateRetrofit()

    fun <T> createApiClient(
        classT: Class<T>,
        serverUrl: String = BuildConfig.BASE_URL
    ): T =
        if (sessionState.baseUrl != serverUrl) {
            sessionState.baseUrl = serverUrl
            retrofitCached = updateRetrofit(serverUrl)
            retrofitCached
        } else {
            retrofitCached
        }.create(classT)

    private fun updateRetrofit(
        serverUrl: String = BuildConfig.BASE_URL
    ): Retrofit =
        Retrofit
            .Builder()
            .apply {
                baseUrl(serverUrl)
                client(okHttpClient)
                addConverterFactory(GsonConverterFactory.create(gson))
                addCallAdapterFactory(CoroutineCallAdapterFactory())
            }
            .build()

    companion object {

        private const val CONNECTION_TIMEOUT: Long = 180L
        private const val READ_TIMEOUT: Long = 180L
        private const val WRITE_TIMEOUT: Long = 180L
    }
}
