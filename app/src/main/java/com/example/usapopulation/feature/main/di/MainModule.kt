package com.example.usapopulation.feature.main.di

import android.content.Context
import com.example.usapopulation.core.data.network.NetworkState
import com.example.usapopulation.feature.main.presentation.router.IMainRouter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.scopes.ActivityScoped

@InstallIn(ActivityComponent::class)
@Module
object MainModule {

    @Provides
    @ActivityScoped
    fun provideMainRouter(
        @ActivityContext activity: Context
    ): IMainRouter = activity as IMainRouter

    @Provides
    @ActivityScoped
    fun provideNetworkState(@ActivityContext activity: Context): NetworkState = NetworkState(activity)
}
