package com.example.usapopulation.feature.main.presentation.viewmodel

import com.example.usapopulation.core.presentation.BaseViewModel
import com.example.usapopulation.feature.main.presentation.router.IMainRouter

class MainViewModel(
    private val mainRouter: IMainRouter
) : BaseViewModel() {

    fun init() {
        mainRouter.openPopulationStatistics()
    }
}
