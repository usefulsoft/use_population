package com.example.usapopulation.feature.main.presentation

import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.usapopulation.R
import com.example.usapopulation.core.presentation.BaseActivity
import com.example.usapopulation.feature.main.presentation.router.IMainRouter
import com.example.usapopulation.feature.main.presentation.viewmodel.MainViewModel
import com.example.usapopulation.feature.statistics.presentation.fragment.PopulationStatisticsFragment
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

/*
Запрос https://datausa.io/api/data?drilldowns=Nation&measures=Population
// TODO Необходимо реализовать выше запрос (авторизация не требуется) и вывести значение на экран ввиде списка элементов и
// TODO сделать возможность сортировать список по годам в порядке возрастания/убывания
Для сокращения времени рутинной работы в package "model" создан валидный для ответ на запрос выше в классе "PopulationResponse".
Также в ApiClient лежит базово настроенный retrofit client.

Желательно но не обязательно:
 1)Реализовать подходы CLEAN архитектуры в процессе написания тестового задания
 2)Разработать обработку сетевых ошибок с выводом дружественного пользователю сообщения на экран
*/

@AndroidEntryPoint
class MainActivity : BaseActivity(), IMainRouter {

    @ActivityScoped
    @Inject
    lateinit var mainRouter: IMainRouter

    private val mainViewModel: MainViewModel by viewModels(
        factoryProducer = {
            object : ViewModelProvider.Factory {
                override fun <T : ViewModel?> create(modelClass: Class<T>): T =
                    MainViewModel(mainRouter) as T
            }
        }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel.init()
    }

    override fun openPopulationStatistics() {
        navigateToFragment(
            PopulationStatisticsFragment.newInstance()
        )
    }

    override fun onBackPressed() {
        when (supportFragmentManager.findFragmentById(R.id.flScreenContainer)) {
            is PopulationStatisticsFragment -> finish()
            else -> super.onBackPressed()
        }
    }

    private fun navigateToFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .replace(R.id.flScreenContainer, fragment)
            .commit()
    }
}
