package com.example.usapopulation.feature.statistics.data.api

import com.example.usapopulation.feature.statistics.data.model.PopulationStatisticsResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface PopulationStatisticsApi {

    @GET("api/data?drilldowns=Nation&measures=Population")
    fun populationStatistics(): Deferred<PopulationStatisticsResponse>
}
