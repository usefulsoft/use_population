package com.example.usapopulation.feature.statistics.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDownward
import androidx.compose.material.icons.filled.ArrowUpward
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.usapopulation.R
import com.example.usapopulation.core.data.network.NetworkState
import com.example.usapopulation.feature.statistics.domain.interactor.PopulationStatisticsInteractor
import com.example.usapopulation.feature.statistics.presentation.viewmodel.PopulationStatisticsViewModel
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.scopes.ActivityScoped
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@AndroidEntryPoint
class PopulationStatisticsFragment : Fragment() {

    @FragmentScoped
    @Inject
    lateinit var populationStatisticsInteractor: PopulationStatisticsInteractor

    @ActivityScoped
    @Inject
    lateinit var networkState: NetworkState

    private val populationStatisticsViewModel: PopulationStatisticsViewModel by viewModels(
        factoryProducer = {
            object : ViewModelProvider.Factory {
                override fun <T : ViewModel?> create(modelClass: Class<T>): T =
                    PopulationStatisticsViewModel(populationStatisticsInteractor, networkState) as T
            }
        }
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setContent {
            ComposeContent()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        populationStatisticsViewModel.init()
    }

    @Composable
    private fun ComposeContent() {

        MaterialTheme {
            Surface(
                color = MaterialTheme.colors.background,
                modifier = Modifier.fillMaxSize()
            ) {
                Column(
                    modifier = Modifier.fillMaxSize()
                ) {

                    with(populationStatisticsViewModel) {

                        if (msProgress.value) {
                            Box(
                                modifier = Modifier.fillMaxSize()
                            ) {
                                CircularProgressIndicator(
                                    modifier = Modifier.align(
                                        alignment = Alignment.Center
                                    )
                                )
                            }
                        }

                        msPopulationStatistics.value?.let {
                            Column {
                                Box(
                                    modifier = Modifier.fillMaxWidth(),
                                    contentAlignment = Alignment.Center
                                ) {
                                    Text(
                                        text = it.sourceDescription,
                                        textAlign = TextAlign.Center,
                                        modifier = Modifier.padding(16.dp)
                                    )
                                }

                                Row(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(16.dp)
                                ) {
                                    Row(
                                        modifier = Modifier
                                            .weight(1f)
                                            .clickable {
                                                populationStatisticsViewModel.toggleYearOrder()
                                            }
                                    ) {
                                        Text(
                                            text = getString(R.string.year),
                                            modifier = Modifier.padding(0.dp, 0.dp, 8.dp, 0.dp)
                                        )

                                        Icon(
                                            imageVector = if (it.yearOrderDescending) {
                                                Icons.Default.ArrowDownward
                                            } else {
                                                Icons.Default.ArrowUpward
                                            },
                                            contentDescription = getString(R.string.year_order)
                                        )
                                    }

                                    Text(
                                        text = getString(R.string.population),
                                        modifier = Modifier.weight(1f)
                                    )

                                    Text(
                                        text = getString(R.string.nation),
                                        modifier = Modifier.weight(1f)
                                    )
                                }

                                LazyColumn {
                                    items(it.data) {
                                        Row(
                                            modifier = Modifier
                                                .fillMaxWidth()
                                                .padding(16.dp)
                                        ) {
                                            Text(
                                                text = it.year,
                                                modifier = Modifier.weight(1f)
                                            )

                                            Text(
                                                text = it.population,
                                                modifier = Modifier.weight(1f)
                                            )

                                            Text(
                                                text = it.nation,
                                                modifier = Modifier.weight(1f)
                                            )
                                        }
                                    }
                                }
                            }
                        }

                        msError.value?.let {
                            Box(
                                modifier = Modifier.fillMaxSize(),
                                contentAlignment = Alignment.Center
                            ) {
                                Column(
                                    horizontalAlignment = Alignment.CenterHorizontally
                                ) {
                                    Text(
                                        text = getString(it),
                                        textAlign = TextAlign.Center
                                    )

                                    Button(
                                        onClick = populationStatisticsViewModel::loadPopulationStatistics,
                                        modifier = Modifier.padding(0.dp, 16.dp)
                                    ) {
                                        Text(
                                            text = getString(R.string.try_again)
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    companion object {

        fun newInstance(): PopulationStatisticsFragment = PopulationStatisticsFragment()
    }
}
