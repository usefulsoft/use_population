package com.example.usapopulation.feature.statistics.presentation.viewmodel

import androidx.compose.runtime.mutableStateOf
import com.example.usapopulation.R
import com.example.usapopulation.core.data.network.NetworkState
import com.example.usapopulation.core.domain.model.DomainError
import com.example.usapopulation.core.presentation.BaseViewModel
import com.example.usapopulation.feature.statistics.domain.interactor.PopulationStatisticsInteractor
import com.example.usapopulation.feature.statistics.domain.model.PopulationStatisticsEntity

class PopulationStatisticsViewModel(
    private val populationStatisticsInteractor: PopulationStatisticsInteractor,
    private val networkState: NetworkState
) : BaseViewModel() {

    val msPopulationStatistics = mutableStateOf<PopulationStatisticsEntity?>(null)

    fun init() {
        loadPopulationStatistics()
    }

    fun loadPopulationStatistics() {
        if (networkState.isNetworkAvailable) {
            jobIO {
                populationStatisticsInteractor.populationStatistics().let {

                    if (it.isFailure) {

                        when (it.exceptionOrNull()) {
                            is DomainError.ConnectionError -> msError.value = R.string.check_internet_connection_error
                            is DomainError.EmptyDataError -> msError.value = R.string.no_data_error
                            else -> msError.value = R.string.service_unavailable_error
                        }
                    } else {

                        msPopulationStatistics.value = it.getOrNull()?.sortedByYear()
                    }
                }
            }
        } else {
            msError.value = R.string.check_internet_connection_error
        }
    }

    fun toggleYearOrder() {
        msPopulationStatistics.value?.yearOrderDescending =
            msPopulationStatistics.value?.yearOrderDescending != true

        msPopulationStatistics.value = msPopulationStatistics.value?.sortedByYear()
    }

    private fun PopulationStatisticsEntity.sortedByYear() =
        copy(
            data = if (this.yearOrderDescending) {
                data.sortedByDescending { it.year }
            } else {
                data.sortedBy { it.year }
            }
        )
}
