package com.example.usapopulation.feature.main.presentation.router

interface IMainRouter {

    fun openPopulationStatistics()
}
