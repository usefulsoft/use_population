package com.example.usapopulation.feature.statistics.di

import com.example.usapopulation.core.data.network.ApiClient
import com.example.usapopulation.feature.statistics.data.api.PopulationStatisticsApi
import com.example.usapopulation.feature.statistics.data.mapper.PopulationStatisticsMapper
import com.example.usapopulation.feature.statistics.data.repository.PopulationStatisticsRepository
import com.example.usapopulation.feature.statistics.domain.interactor.PopulationStatisticsInteractor
import com.example.usapopulation.feature.statistics.domain.repository.IPopulationStatisticsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.scopes.FragmentScoped

@InstallIn(FragmentComponent::class)
@Module
object PopulationStatisticsModule {

    @Provides
    @FragmentScoped
    fun providePopulationStatisticsApi(
        apiClient: ApiClient
    ): PopulationStatisticsApi = apiClient.createApiClient(PopulationStatisticsApi::class.java)

    @Provides
    @FragmentScoped
    fun providePopulationStatisticsMapper(): PopulationStatisticsMapper = PopulationStatisticsMapper()

    @Provides
    @FragmentScoped
    fun providePopulationStatisticsRepository(
        populationStatisticsApi: PopulationStatisticsApi,
        populationStatisticsMapper: PopulationStatisticsMapper
    ): IPopulationStatisticsRepository =
        PopulationStatisticsRepository(
            populationStatisticsApi, populationStatisticsMapper
        )

    @Provides
    @FragmentScoped
    fun providePopulationStatisticsInteractor(
        populationStatisticsRepository: IPopulationStatisticsRepository
    ): PopulationStatisticsInteractor =
        PopulationStatisticsInteractor(populationStatisticsRepository)
}
